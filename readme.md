this repo is to show an issue with launchagents running on top of the login window. To replicate the issue:

1. Compile and copy the LoginAgent.app to /Library/PrivilegedHelperTools/
2. add the com.research.loginagent.plist to /Library/LaunchAgents/
3. Log out and note the red "hello" in upper right corner. 
4. Reboot and note that the red hello does not appear.
5. Remove com.research.loginagent.plist and add com.research.loginagent-wait5.plist to /Library/LaunchAgents/
6. Log out and note the red "hello" in upper right corner. 
7. Reboot and note that the red hello now appears.

The only different is the sleep 10.

The log does show this error:

2021-05-17 16:26:32.557858-0500 0x9f4      Error       0x0                  403    7    LoginAgent: (SkyLight) [com.apple.SkyLight:default] This user is not allowed access to the window system right now.

