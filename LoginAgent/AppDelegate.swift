//
//  AppDelegate.swift
//  LoginAgent
//

import AppKit
import SwiftUI

class AppDelegate: NSObject, NSApplicationDelegate {
    var window: NSWindow!

    func applicationDidFinishLaunching(_ aNotification: Notification) {
        let contentView = ContentView()

        window = NSWindow(
            contentRect: NSRect(x: 0, y: 0, width: 150, height: 100),
            styleMask: .borderless, backing: .buffered, defer: false)
        window.canBecomeVisibleWithoutLogin = true
        window.contentView = NSHostingView(rootView: contentView)
        window.backgroundColor = .red

        if let screenFrame = NSScreen.main?.frame {
            let windowFrame = window.frame
            let x = screenFrame.width - windowFrame.width
            let y = screenFrame.height
            let point = NSPoint(x: x, y: y)
            window.setFrameTopLeftPoint(point)
        } else {
            window.center()
        }

        window.orderFrontRegardless()
    }
}
