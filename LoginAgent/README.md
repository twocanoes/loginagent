#  LoginAgent
This is a hello world pre-login launch agent.

## Installation
Build `LoginAgent.app` and copy it to `/Library/PrivilegedHelperTools`
Copy `com.research.loginagent.plist` to `/Library/LaunchAgents`

## Expected behavior
Upon logout or restart, 'Hello' text is displayed on login screen

## Actual behavior
When user logs out, 'Hello' text is indeed displayed on the login screen
However, when user restarts computer (Intel or M1), 'Hello' text is not displayed on the login screen
